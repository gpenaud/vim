"------------------------------------------------------------
"----------------- Lancement de pathogen --------------------
execute pathogen#infect()
syntax on
filetype plugin indent on

"------------------------------------------------------------
"-------------- Démarrage automatique de nerdtree -----------
function! StartUp()
    if 0 == argc()
        NERDTree
    end
endfunction

autocmd VimEnter * call StartUp()

"------------------------------------------------------------
"-------------- Démarrage automatique de nerdtree -----------
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set number 

:set guioptions-=m  "remove menu bar
:set guioptions-=T  "remove toolbar
:set guioptions-=r  "remove right-hand scroll bar
:set guioptions-=L  "remove left-hand scroll bar

"------------------------------------------------------------
"--------------- Ajout de la configuration LaTex ------------

" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin on

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
set shellslash

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: This enables automatic indentation as you type.
filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

"colorscheme molokai
"let g:molokai_original = 1
"set background=dark

set t_Co=256
colorscheme jellybeans

"--------------------------------------------------------
"-------- leader definition -----------------------------

let mapleader=","

"--------------------------------------------------------
"-------------- keybindings -----------------------------

:imap ;; <Esc>
